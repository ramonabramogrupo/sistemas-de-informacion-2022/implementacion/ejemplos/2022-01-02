﻿USE ventas1;

-- MIRAMOS EL CONTENIDO DE LAS TABLAS

SELECT * FROM grupos g;
SELECT * FROM productos p;
SELECT * FROM vendedores v;
SELECT * FROM ventas v;

-- REALIZAMOS UNA COPIA DE SEGURIDAD

-- EJEMPLO 1
-- CONSULTA DE ACTUALIZACION
-- Colocar el total de todas las ventas en 
-- un campo nuevo en la tabla ventas como
-- total=kilos*precioProducto

-- AÑADIMOS A LA TABLA VENTAS UN CAMPO TOTAL
ALTER TABLE ventas
  ADD COLUMN total float DEFAULT 0;

SELECT * FROM  ventas v;
SELECT * FROM productos p JOIN ventas v1 ON p.IdProducto = v1.`Cod Producto`;

-- LA CONSULTA SERIA
UPDATE 
  productos p JOIN ventas v ON p.IdProducto = v.`Cod Producto`
  SET v.total=v.Kilos*p.Precio;

-- consulta 2

UPDATE 
  grupos g 
  JOIN productos p ON g.IdGrupo = p.IdGrupo
  JOIN ventas v ON p.IdProducto = v.`Cod Producto`
  SET v.total=v.total*0.9
  WHERE g.NombreGrupo='Verduras';


-- consulta 3
-- crear una tabla llamada verduras con 
-- el nombre de todos los productos que sean verduras

DROP TABLE IF EXISTS verduras;

CREATE TABLE verduras(
  id int AUTO_INCREMENT,
  NomProducto varchar(100),
  PRIMARY KEY(id)
) AS 
  SELECT p.NomProducto 
    FROM productos p JOIN grupos g ON p.IdGrupo = g.IdGrupo
    WHERE g.NombreGrupo='verduras';

SELECT * FROM verduras v;

-- consulta 4
-- La misma consulta anterior
-- PODEMOS REALIZARLA EN DOS
-- Creacion de tabla + Datos anexados

DROP TABLE IF EXISTS verduras;

CREATE TABLE verduras(
  id int AUTO_INCREMENT,
  NomProducto varchar(100),
  PRIMARY KEY(id)
  );

-- NECESITARIA UNA CONSULTA DE DATOS ANEXADOS

INSERT INTO verduras (NomProducto)
  SELECT p.NomProducto 
    FROM grupos g JOIN productos p ON g.IdGrupo = p.IdGrupo
    WHERE g.NombreGrupo="verduras";

SELECT * FROM verduras v;
  
-- consulta 5
-- Realizar esta consulta con 
-- Crear una tabla llamada productosVendidos
-- donde me coloque el siguiente resultado
-- nombreGrupo,numeroProductosVendidos
-- verduras,45 (hay 45 verduras en la tabla ventas)

-- creacion de tabla (5.1) 
-- Create table .... as select ...
-- datos anexados (5.2)
-- primero creo la tabla con create table 
-- Insert into ..... select ....


-- 5.1

DROP TABLE IF EXISTS productosVendidos;

CREATE TABLE productosVendidos(
  id int AUTO_INCREMENT,
  nombreGrupo varchar(100),
  numeroProductosVendidos int,
  PRIMARY KEY(id)
) AS 
  SELECT 
      g.NombreGrupo,
      COUNT(*) AS numeroProductosVendidos 
    FROM grupos g JOIN productos p ON g.IdGrupo = p.IdGrupo
      JOIN ventas v ON p.IdProducto = v.`Cod Producto`
    GROUP BY g.NombreGrupo;

SELECT * FROM productosVendidos v;

-- 5.2

DROP TABLE IF EXISTS productosVendidos;

CREATE TABLE productosVendidos(
  id int AUTO_INCREMENT,
  nombreGrupo varchar(100),
  numeroProductosVendidos int,
  PRIMARY KEY(id));

INSERT INTO productosvendidos (nombreGrupo, numeroProductosVendidos)
  SELECT 
      g.NombreGrupo, 
      COUNT(*) 
    FROM grupos g 
      JOIN productos p ON g.IdGrupo = p.IdGrupo
      JOIN ventas v ON p.IdProducto = v.`Cod Producto`
    GROUP BY g.NombreGrupo;
      

SELECT * FROM productosVendidos v;

SELECT * FROM ventas v;

-- CONSULTA 6
-- ELIMINACION 
-- ELIMINAR LAS VENTAS 
-- CUYOS KILOS ESTAN ENTRE 300 Y 500

DELETE 
  FROM ventas 
  WHERE Kilos BETWEEN 300 AND 500;


-- CONSULTA 7
-- ELIMINAR LAS VENTAS DE LAS LECHUGAS

-- sintaxis 1
DELETE  v -- tabla donde borro datos
  FROM productos  
    JOIN ventas v ON productos.IdProducto = v.`Cod Producto`
    WHERE NomProducto='Lechugas';

SELECT * FROM productos p;

-- sintaxis 2
DELETE  
  FROM v -- tabla donde elimino los registros 
    USING productos  
    JOIN ventas v ON productos.IdProducto = v.`Cod Producto`
    WHERE NomProducto='Lechugas';


